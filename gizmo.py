""" Quickstart script for InstaPy usage """
# imports
from instapy import InstaPy
from instapy.util import smart_run
import os



# login credentials
insta_username = os.environ['INSTA_USERNAME']
insta_password = os.environ['INSTA_PASSWORD']

# get an InstaPy session!
# set headless_browser=True to run InstaPy in the background
session = InstaPy(username=insta_username,
                  password=insta_password,
                  headless_browser=True,
                  show_logs=False)


with smart_run(session):
    """ Activity flow """
    # general settings
    session.set_relationship_bounds(enabled=False,
                                      delimit_by_numbers=True,
                                       max_followers=4590,
                                        min_followers=10,
                                        min_following=10)

    #session.set_comments(["Cool!", "So Cute!", "*AMAZED*"])
    #session.like_by_tags(["shiba", "shibainu"], amount=250)
    #session.set_user_interact(amount=250, randomize=True, percentage=50, media='Photo')
    session.follow_user_followers(['yumohiyo', 'shiba_charmy', 'ganchi_with_kotetsu', 'mamemarukun', 'mamechiyo728', 'daifuku_channel'], amount=250, randomize=True, interact=True)
    session.set_dont_unfollow_active_users(enabled=True, posts=5)
    session.unfollow_users(amount=500, nonFollowers=True, style="RANDOM", unfollow_after=42*60*60, sleep_delay=5)

